import React from "react";
import { Layout, Menu } from "antd";
import { HeartOutlined, HomeOutlined } from "@ant-design/icons";
import { Link } from "react-router-dom";

interface Props {
  children: JSX.Element;
}

const { Header, Footer, Content } = Layout;

const menuItems = [
  { label: <Link to="/">Domov</Link>, key: "menu-1", icon: <HomeOutlined /> },
  {
    label: <Link to="favorites">Moje obľúbené</Link>,
    key: "menu-2",
    icon: <HeartOutlined />,
  },
];

const BasicLayout = ({ children }: Props) => {
  return (
    <Layout>
      <Layout>
        <Header style={{ color: "#fff" }}>
          <div>
            <div
              style={{
                float: "left",
              }}
            >
              <Link to="/">
                <h1 style={{ color: "#fff", fontWeight: "bold" }}>Movie App</h1>
              </Link>
            </div>
            <Menu
              theme="dark"
              mode="horizontal"
              defaultSelectedKeys={["1"]}
              items={menuItems}
              style={{ float: "right" }}
            />
          </div>
        </Header>
        <Content style={{ margin: "24px 16px 0" }}>
          <div style={{ padding: 24, background: "#fff", minHeight: 360 }}>
            {children}
          </div>
        </Content>
        <Footer style={{ textAlign: "center" }}>
          Movie App ©2022 Created by Milan Jakubec
        </Footer>
      </Layout>
    </Layout>
  );
};

export default BasicLayout;
