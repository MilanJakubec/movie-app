import { IMovieSearch } from "../app/store/movieSearch/types";

export const insetToLocalStorage = (
  favorites: IMovieSearch[],
  movie: IMovieSearch
): void => {
  const ids = favorites.map((favorite) => favorite.imdbID);

  localStorage.setItem("favorites", JSON.stringify(ids));
  localStorage.setItem("favoriteItem-" + movie.imdbID, JSON.stringify(movie));
};

export const removeFromLocalStorage = (
  favorites: IMovieSearch[],
  movie: IMovieSearch
): void => {
  const ids = favorites.map((favorite) => favorite.imdbID);

  localStorage.setItem("favorites", JSON.stringify(ids));
  localStorage.removeItem("favoriteItem-" + movie.imdbID);
};

export const getAllMovieLocalStorage = (): IMovieSearch[] | [] => {
  const storageFavorites = localStorage.getItem("favorites");

  if (storageFavorites === null) {
    return [];
  }

  const favoritesArr: string[] = JSON.parse(storageFavorites || "");

  let favorites: IMovieSearch[] = [];

  if (favoritesArr.length > 0) {
    favorites = favoritesArr.map((imdbID) =>
      JSON.parse(localStorage.getItem("favoriteItem-" + imdbID) || "")
    );
  }

  if (favorites.length > 0) {
    return favorites;
  } else {
    return [];
  }
};
