interface Rating {
  Source: string;
  Value: string;
}

interface Props {
  rated: string;
  imdbRating: string;
  imdbVotes: string;
  ratings: Rating[];
  metaScore: string;
}

const RatingDescription = ({
  rated,
  imdbRating,
  imdbVotes,
  ratings,
  metaScore,
}: Props) => {
  return (
    <div className="ant-descriptions">
      <div className="ant-descriptions-header">
        <div className="ant-descriptions-title">Ratings</div>
      </div>
      <div className="ant-descriptions-view">
        <table>
          <tbody>
            <tr className="ant-descriptions-row">
              <td className="ant-descriptions-item" colSpan={1}>
                <div className="ant-descriptions-item-container">
                  <span
                    className="ant-descriptions-item-label"
                    style={{ fontWeight: "bold" }}
                  >
                    Rated
                  </span>
                  <span className="ant-descriptions-item-content">{rated}</span>
                </div>
              </td>
              <td className="ant-descriptions-item" colSpan={1}>
                <div className="ant-descriptions-item-container">
                  <span
                    className="ant-descriptions-item-label"
                    style={{ fontWeight: "bold" }}
                  >
                    Imdb Rating
                  </span>
                  <span className="ant-descriptions-item-content">
                    {imdbRating}
                  </span>
                </div>
              </td>
              <td className="ant-descriptions-item" colSpan={1}>
                <div className="ant-descriptions-item-container">
                  <span
                    className="ant-descriptions-item-label"
                    style={{ fontWeight: "bold" }}
                  >
                    Imdb Votes
                  </span>
                  <span className="ant-descriptions-item-content">
                    {imdbVotes}
                  </span>
                </div>
              </td>
            </tr>

            <tr className="ant-descriptions-row">
              {ratings.map((rating, index) => {
                return (
                  <td
                    className="ant-descriptions-item"
                    colSpan={1}
                    key={rating.Source}
                  >
                    <div className="ant-descriptions-item-container">
                      <span
                        className="ant-descriptions-item-label"
                        style={{ fontWeight: "bold" }}
                      >
                        {rating.Source}
                      </span>
                      <span className="ant-descriptions-item-content">
                        {rating.Value}
                      </span>
                    </div>
                  </td>
                );
              })}
            </tr>
            <tr className="ant-descriptions-row">
              <td className="ant-descriptions-item" colSpan={1}>
                <div className="ant-descriptions-item-container">
                  <span
                    className="ant-descriptions-item-label"
                    style={{ fontWeight: "bold" }}
                  >
                    Meta score
                  </span>
                  <span className="ant-descriptions-item-content">
                    {metaScore}
                  </span>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default RatingDescription;
