import { Card, Divider, List, Spin } from "antd";
import InfiniteScroll from "react-infinite-scroll-component";
import { Link, useNavigate } from "react-router-dom";
import { HeartFilled, HeartOutlined } from "@ant-design/icons";
import React from "react";
import { IMovieSearch } from "../app/store/movieSearch/types";
import ConditionalWrapper from "./ConditionalWrapper";

interface Props {
  totalResults: number;
  page: number;
  favorites: IMovieSearch[];
  data: IMovieSearch[];
  loadMore: () => void;
  handleIconClick: (movies: IMovieSearch) => void;
}

const MovieCard = ({
  totalResults,
  page,
  favorites,
  data,
  loadMore,
  handleIconClick,
}: Props) => {
  let navigate = useNavigate();
  const handleImageClick = (
    e: React.MouseEvent<HTMLImageElement, MouseEvent>,
    imdbID: string
  ): void => {
    e.preventDefault();

    navigate("detail/" + imdbID);
  };

  return (
    <Card>
      <div
        id="scrollableDiv"
        style={{
          height: "100vh",
          overflow: "auto",
        }}
      >
        <ConditionalWrapper
          condition={page !== 0}
          wrapper={(children) => (
            <InfiniteScroll
              dataLength={totalResults}
              next={loadMore}
              hasMore={page * 10 < totalResults}
              loader={<Spin />}
              endMessage={<Divider plain>It is all, nothing more</Divider>}
              scrollableTarget="scrollableDiv"
            >
              {children}
            </InfiniteScroll>
          )}
          children={
            <List
              grid={{
                gutter: 16,
                xs: 1,
                sm: 2,
                md: 4,
                lg: 4,
                xl: 6,
                xxl: 3,
              }}
              dataSource={data}
              renderItem={(item) => (
                <List.Item key={item.imdbID}>
                  <Card
                    hoverable
                    cover={
                      <img
                        alt={item.Title}
                        src={
                          item.Poster !== "N/A"
                            ? item.Poster
                            : "/No-Image-Placeholder.svg"
                        }
                        onClick={(event) =>
                          handleImageClick(event, item.imdbID)
                        }
                      />
                    }
                  >
                    <Link to={`/detail/${item.imdbID}`}>
                      <h3>{item.Title}</h3>
                    </Link>
                    <p>
                      Type: {item.Type}
                      <br></br>
                      Year: {item.Year}
                    </p>
                    <Divider />
                    <div>
                      {favorites.includes(item) ? (
                        <HeartFilled onClick={() => handleIconClick(item)} />
                      ) : (
                        <HeartOutlined onClick={() => handleIconClick(item)} />
                      )}
                    </div>
                  </Card>
                </List.Item>
              )}
            />
          }
        />
      </div>
    </Card>
  );
};

export default MovieCard;
