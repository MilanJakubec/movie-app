import React from "react";
import "../../App.css";
import { Outlet } from "react-router-dom";
import BasicLayout from "../../layout/BasicLayout";

const App = () => {
  return <BasicLayout children={<Outlet />} />;
};

export default App;
