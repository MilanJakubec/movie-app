import {
  FETCH_MOVIE_DETAIL_FAILURE,
  FETCH_MOVIE_DETAIL_REQUEST,
  FETCH_MOVIE_DETAIL_SUCCESS,
} from "./actionTypes";
import { MovieDetailActions, MovieDetailState } from "./types";

const initialState: MovieDetailState = {
  movieDetail: {
    pending: false,
    movieDetail: null,
    error: null,
  },
};

export default (state = initialState, action: MovieDetailActions) => {
  switch (action.type) {
    case FETCH_MOVIE_DETAIL_REQUEST:
      return {
        ...state,
        movieDetail: {
          pending: true,
          movieDetail: null,
          error: null,
        },
      };
    case FETCH_MOVIE_DETAIL_SUCCESS:
      return {
        ...state,
        movieDetail: {
          pending: false,
          movieDetail: action.payload.movieDetail,
          error: null,
        },
      };
    case FETCH_MOVIE_DETAIL_FAILURE:
      return {
        ...state,
        movieDetail: {
          pending: false,
          movieDetail: null,
          error: action.payload.error,
        },
      };
    default:
      return {
        ...state,
      };
  }
};
