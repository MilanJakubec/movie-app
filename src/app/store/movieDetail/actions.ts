import {
  FetchMovieDetailFailure,
  FetchMovieDetailFailurePayload,
  FetchMovieDetailRequest,
  FetchMovieDetailRequestPayload,
  FetchMovieDetailSuccess,
  FetchMovieDetailSuccessPayload,
} from "./types";
import {
  FETCH_MOVIE_DETAIL_FAILURE,
  FETCH_MOVIE_DETAIL_REQUEST,
  FETCH_MOVIE_DETAIL_SUCCESS,
} from "./actionTypes";

export const fetchMovieDetailRequest = (
  payload: FetchMovieDetailRequestPayload
): FetchMovieDetailRequest => ({
  type: FETCH_MOVIE_DETAIL_REQUEST,
  payload,
});

export const fetchMovieDetailSuccess = (
  payload: FetchMovieDetailSuccessPayload
): FetchMovieDetailSuccess => ({
  type: FETCH_MOVIE_DETAIL_SUCCESS,
  payload,
});

export const fetchMovieDetailFailure = (
  payload: FetchMovieDetailFailurePayload
): FetchMovieDetailFailure => ({
  type: FETCH_MOVIE_DETAIL_FAILURE,
  payload,
});
