import { all, call, put, takeLatest } from "redux-saga/effects";
import { fetchMovieDetailFailure, fetchMovieDetailSuccess } from "./actions";
import { FETCH_MOVIE_DETAIL_REQUEST } from "./actionTypes";
import { FetchMovieDetailRequest, IMovieDetail } from "./types";
import axios, { AxiosResponse } from "axios";

const getMovieDetail = (imdbID: string): ReturnType<any> => {
  return axios.get<IMovieDetail>(
    `http://omdbapi.com/?apikey=${process.env.REACT_APP_OMDBAPI_KEY}&i=${imdbID}`
  );
};

function* fetchMovieDetailSaga({ payload }: FetchMovieDetailRequest) {
  try {
    const response: AxiosResponse<IMovieDetail> = yield call(
      getMovieDetail,
      payload.imdbID
    );

    yield put(
      fetchMovieDetailSuccess({
        movieDetail: response.data,
      })
    );
  } catch (e) {
    let errorMessage = "Failed to fetch movie detail.";
    if (e instanceof Error) {
      errorMessage = e.message;
    }

    yield put(
      fetchMovieDetailFailure({
        error: errorMessage,
      })
    );
  }
}

function* movieDetailSaga() {
  yield all([takeLatest(FETCH_MOVIE_DETAIL_REQUEST, fetchMovieDetailSaga)]);
}

export default movieDetailSaga;
