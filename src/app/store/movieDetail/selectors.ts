import { createSelector } from "reselect";

import { AppState } from "../rootReducer";
import { MovieDetailState } from "./types";

const getMovieDetail = (state: AppState) => state.movieDetail;

export const getMovieDetailSelector = createSelector(
  getMovieDetail,
  (movieDetail: MovieDetailState) => movieDetail
);
