import {
  FETCH_MOVIE_DETAIL_FAILURE,
  FETCH_MOVIE_DETAIL_REQUEST,
  FETCH_MOVIE_DETAIL_SUCCESS,
} from "./actionTypes";

interface Rating {
  Source: string;
  Value: string;
}

export interface IMovieDetail {
  Title: string;
  Year: string;
  Rated: string;
  Released: string;
  Runtime: string;
  Genre: string;
  Director: string;
  Writer: string;
  Actors: string;
  Plot: string;
  Language: string;
  Country: string;
  Awards: string;
  Poster: string;
  Ratings: Rating[];
  Metascore: string;
  imdbRating: string;
  imdbVotes: string;
  imdbID: string;
  Type: string;
  DVD: string;
  BoxOffice: string;
  Production: string;
  Website: string;
  Response: string;
}

export interface MovieDetailState {
  movieDetail: {
    pending: boolean;
    movieDetail: IMovieDetail | null;
    error: string | null;
  };
}

export interface FetchMovieDetailRequestPayload {
  imdbID: string;
}

export interface FetchMovieDetailSuccessPayload {
  movieDetail: IMovieDetail;
}

export interface FetchMovieDetailFailurePayload {
  error: string;
}

export interface FetchMovieDetailRequest {
  type: typeof FETCH_MOVIE_DETAIL_REQUEST;
  payload: FetchMovieDetailRequestPayload;
}

export interface FetchMovieDetailSuccess {
  type: typeof FETCH_MOVIE_DETAIL_SUCCESS;
  payload: FetchMovieDetailSuccessPayload;
}

export interface FetchMovieDetailFailure {
  type: typeof FETCH_MOVIE_DETAIL_FAILURE;
  payload: FetchMovieDetailFailurePayload;
}

export type MovieDetailActions =
  | FetchMovieDetailRequest
  | FetchMovieDetailSuccess
  | FetchMovieDetailFailure;
