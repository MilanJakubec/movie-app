import {
  DELETE_MOVIE_SEARCH,
  FETCH_MORE_MOVIES_FAILURE,
  FETCH_MORE_MOVIES_REQUEST,
  FETCH_MORE_MOVIES_SUCCESS,
  FETCH_MOVIE_SEARCH_FAILURE,
  FETCH_MOVIE_SEARCH_REQUEST,
  FETCH_MOVIE_SEARCH_SUCCESS,
} from "./actionTypes";
import {
  DeleteMovieSearch,
  FetchMoreMoviesFailure,
  FetchMoreMoviesRequest,
  FetchMoreMoviesSuccess,
  FetchMovieSearchFailure,
  FetchMovieSearchFailurePayload,
  FetchMovieSearchRequest,
  FetchMovieSearchRequestPayload,
  FetchMovieSearchSuccess,
  FetchMovieSearchSuccessPayload,
} from "./types";

export const fetchMovieSearchRequest = (
  payload: FetchMovieSearchRequestPayload
): FetchMovieSearchRequest => ({
  type: FETCH_MOVIE_SEARCH_REQUEST,
  payload,
});

export const fetchMovieSearchSuccess = (
  payload: FetchMovieSearchSuccessPayload
): FetchMovieSearchSuccess => ({
  type: FETCH_MOVIE_SEARCH_SUCCESS,
  payload,
});

export const fetchMovieSearchFailure = (
  payload: FetchMovieSearchFailurePayload
): FetchMovieSearchFailure => ({
  type: FETCH_MOVIE_SEARCH_FAILURE,
  payload,
});

export const fetchMoreMoviesRequest = (
  payload: FetchMovieSearchRequestPayload
): FetchMoreMoviesRequest => ({
  type: FETCH_MORE_MOVIES_REQUEST,
  payload,
});

export const fetchMoreMoviesSuccess = (
  payload: FetchMovieSearchSuccessPayload
): FetchMoreMoviesSuccess => ({
  type: FETCH_MORE_MOVIES_SUCCESS,
  payload,
});

export const fetchMoreMoviesFailure = (
  payload: FetchMovieSearchFailurePayload
): FetchMoreMoviesFailure => ({
  type: FETCH_MORE_MOVIES_FAILURE,
  payload,
});

export const deleteMovieSearch = (): DeleteMovieSearch => ({
  type: DELETE_MOVIE_SEARCH,
});
