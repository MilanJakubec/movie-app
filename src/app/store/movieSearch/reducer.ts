import {
  DELETE_MOVIE_SEARCH,
  FETCH_MORE_MOVIES_FAILURE,
  FETCH_MORE_MOVIES_REQUEST,
  FETCH_MORE_MOVIES_SUCCESS,
  FETCH_MOVIE_SEARCH_FAILURE,
  FETCH_MOVIE_SEARCH_REQUEST,
  FETCH_MOVIE_SEARCH_SUCCESS,
} from "./actionTypes";
import { MovieSearchActions, MovieSearchState } from "./types";

const initialState: MovieSearchState = {
  movieSearch: {
    pending: false,
    movieSearch: [],
    totalResults: 0,
    error: null,
  },
};

export default (state = initialState, action: MovieSearchActions) => {
  switch (action.type) {
    case FETCH_MOVIE_SEARCH_REQUEST:
      return {
        ...state,
        movieSearch: {
          pending: true,
          movieSearch: [],
          totalResults: 0,
          error: null,
        },
      };
    case FETCH_MOVIE_SEARCH_SUCCESS:
      return {
        ...state,
        movieSearch: {
          pending: false,
          movieSearch: action.payload.Search,
          totalResults: action.payload.totalResults,
          error: null,
        },
      };
    case FETCH_MOVIE_SEARCH_FAILURE:
      return {
        ...state,
        movieSearch: {
          pending: false,
          movieSearch: [],
          totalResults: 0,
          error: action.payload.error,
        },
      };
    case FETCH_MORE_MOVIES_REQUEST:
      return {
        ...state,
        movieSearch: {
          pending: true,
          movieSearch: state.movieSearch.movieSearch,
          totalResults: state.movieSearch.totalResults,
          error: null,
        },
      };
    case FETCH_MORE_MOVIES_SUCCESS:
      return {
        ...state,
        movieSearch: {
          pending: false,
          movieSearch: [
            ...state.movieSearch.movieSearch,
            ...action.payload.Search,
          ],
          totalResults: state.movieSearch.totalResults,
          error: null,
        },
      };
    case FETCH_MORE_MOVIES_FAILURE:
      return {
        ...state,
        movieSearch: {
          pending: false,
          movieSearch: state.movieSearch.movieSearch,
          totalResults: state.movieSearch.totalResults,
          error: action.payload.error,
        },
      };
    case DELETE_MOVIE_SEARCH:
      return {
        ...state,
        movieSearch: {
          pending: false,
          movieSearch: [],
          totalResults: 0,
          error: null,
        },
      };

    default:
      return {
        ...state,
      };
  }
};
