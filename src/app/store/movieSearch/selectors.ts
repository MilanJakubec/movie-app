import { createSelector } from "reselect";

import { AppState } from "../rootReducer";
import { MovieSearchState } from "./types";

const getMovieSearch = (state: AppState) => state.movieSearch;

export const getMovieSearchSelector = createSelector(
  getMovieSearch,
  (movieSearch: MovieSearchState) => movieSearch
);
