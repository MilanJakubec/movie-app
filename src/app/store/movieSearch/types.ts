import {
  DELETE_MOVIE_SEARCH,
  FETCH_MORE_MOVIES_FAILURE,
  FETCH_MORE_MOVIES_REQUEST,
  FETCH_MORE_MOVIES_SUCCESS,
  FETCH_MOVIE_SEARCH_FAILURE,
  FETCH_MOVIE_SEARCH_REQUEST,
  FETCH_MOVIE_SEARCH_SUCCESS,
} from "./actionTypes";

export interface IMovieSearch {
  Title: string;
  Year: string;
  imdbID: string;
  Type: string;
  Poster: string;
}

export interface MovieSearchState {
  movieSearch: {
    pending: boolean;
    movieSearch: IMovieSearch[] | [];
    totalResults: number;
    error: string | null;
  };
}

export interface FetchMovieSearchRequestPayload {
  searchText: string;
  page: number;
}

export interface FetchMovieSearchSuccessPayload {
  Response: string;
  Search: IMovieSearch[] | [];
  totalResults: string;
}

export interface FetchMovieSearchFailurePayload {
  error: string;
}

export interface FetchMovieSearchRequest {
  type: typeof FETCH_MOVIE_SEARCH_REQUEST;
  payload: FetchMovieSearchRequestPayload;
}

export interface FetchMovieSearchSuccess {
  type: typeof FETCH_MOVIE_SEARCH_SUCCESS;
  payload: FetchMovieSearchSuccessPayload;
}

export interface FetchMovieSearchFailure {
  type: typeof FETCH_MOVIE_SEARCH_FAILURE;
  payload: FetchMovieSearchFailurePayload;
}

export interface FetchMoreMoviesRequest {
  type: typeof FETCH_MORE_MOVIES_REQUEST;
  payload: FetchMovieSearchRequestPayload;
}

export interface FetchMoreMoviesSuccess {
  type: typeof FETCH_MORE_MOVIES_SUCCESS;
  payload: FetchMovieSearchSuccessPayload;
}

export interface FetchMoreMoviesFailure {
  type: typeof FETCH_MORE_MOVIES_FAILURE;
  payload: FetchMovieSearchFailurePayload;
}

export interface DeleteMovieSearch {
  type: typeof DELETE_MOVIE_SEARCH;
}

export type MovieSearchActions =
  | FetchMovieSearchRequest
  | FetchMovieSearchSuccess
  | FetchMovieSearchFailure
  | FetchMoreMoviesRequest
  | FetchMoreMoviesSuccess
  | FetchMoreMoviesFailure
  | DeleteMovieSearch;
