import axios, { AxiosResponse } from "axios";
import { all, call, put, takeLatest } from "redux-saga/effects";
import {
  FETCH_MORE_MOVIES_REQUEST,
  FETCH_MOVIE_SEARCH_REQUEST,
} from "./actionTypes";
import {
  fetchMoreMoviesFailure,
  fetchMoreMoviesSuccess,
  fetchMovieSearchFailure,
  fetchMovieSearchSuccess,
} from "./actions";
import {
  FetchMovieSearchRequest,
  FetchMovieSearchSuccessPayload,
} from "./types";

const getMovieSearch = (searchText: string, page: number): ReturnType<any> => {
  return axios.get<FetchMovieSearchSuccessPayload>(
    `http://omdbapi.com/?apikey=${process.env.REACT_APP_OMDBAPI_KEY}&s=${searchText}&page=${page}`
  );
};

function* fetchMovieSearchSaga({ payload }: FetchMovieSearchRequest) {
  try {
    const response: AxiosResponse<FetchMovieSearchSuccessPayload> = yield call(
      getMovieSearch,
      payload.searchText,
      payload.page
    );

    yield put(
      fetchMovieSearchSuccess({
        Response: response.data.Response,
        Search: response.data.Search,
        totalResults: response.data.totalResults,
      })
    );
  } catch (e) {
    let errorMessage = "Failed to fetch movie movies.";
    if (e instanceof Error) {
      errorMessage = e.message;
    }

    yield put(
      fetchMovieSearchFailure({
        error: errorMessage,
      })
    );
  }
}

function* fetchMoreMovies({ payload }: FetchMovieSearchRequest) {
  try {
    const response: AxiosResponse<FetchMovieSearchSuccessPayload> = yield call(
      getMovieSearch,
      payload.searchText,
      payload.page
    );

    yield put(
      fetchMoreMoviesSuccess({
        Response: response.data.Response,
        Search: response.data.Search,
        totalResults: response.data.totalResults,
      })
    );
  } catch (e) {
    let errorMessage = "Failed to fetch movie movies.";
    if (e instanceof Error) {
      errorMessage = e.message;
    }

    yield put(
      fetchMoreMoviesFailure({
        error: errorMessage,
      })
    );
  }
}

function* movieSearchSaga() {
  yield all([
    takeLatest(FETCH_MOVIE_SEARCH_REQUEST, fetchMovieSearchSaga),
    takeLatest(FETCH_MORE_MOVIES_REQUEST, fetchMoreMovies),
  ]);
}

export default movieSearchSaga;
