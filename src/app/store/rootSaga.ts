import { all, fork } from "redux-saga/effects";

import movieDetailSaga from "./movieDetail/sagas";
import movieSearchSaga from "./movieSearch/sagas";

export default function* rootSaga() {
  yield all([fork(movieDetailSaga), fork(movieSearchSaga)]);
}
