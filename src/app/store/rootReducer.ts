import { combineReducers } from "@reduxjs/toolkit";
import movieDetailReducer from "./movieDetail/reducer";
import movieSearchReducer from "./movieSearch/reducer";

const rootReducer = combineReducers({
  movieDetail: movieDetailReducer,
  movieSearch: movieSearchReducer,
});

export type AppState = ReturnType<typeof rootReducer>;

export default rootReducer;
