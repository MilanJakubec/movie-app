import ContentLayout from "../layout/ContentLayout";
import { Card, Empty, Input, Result, Spin } from "antd";
import React, { useState } from "react";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { getMovieSearchSelector } from "../app/store/movieSearch/selectors";
import {
  deleteMovieSearch,
  fetchMoreMoviesRequest,
  fetchMovieSearchRequest,
} from "../app/store/movieSearch/actions";
import { IMovieSearch } from "../app/store/movieSearch/types";
import MovieCard from "../components/MovieCard";
import {
  getAllMovieLocalStorage,
  insetToLocalStorage,
  removeFromLocalStorage,
} from "../utils/localStorage";

const { Search } = Input;

const Home = () => {
  const [page, setPage] = useState<number>(1);
  const [searchText, setSearchText] = useState<string>("");
  const [favorites, setFavorites] = useState<IMovieSearch[]>(
    getAllMovieLocalStorage()
  );

  const dispatch = useDispatch();
  const { error, movieSearch, pending, totalResults } = useSelector(
    getMovieSearchSelector
  ).movieSearch;

  const handleIconClick = (movie: IMovieSearch): void => {
    const exist = favorites.some(
      (favorite) => favorite.imdbID === movie.imdbID
    );
    if (exist) {
      const tmpFavorites = favorites.filter(
        (favorite) => favorite.imdbID !== movie.imdbID
      );
      setFavorites(tmpFavorites);

      removeFromLocalStorage(tmpFavorites, movie);
    } else {
      const tmpFavorites = favorites;
      tmpFavorites.push(movie);
      setFavorites([...tmpFavorites]);

      insetToLocalStorage(tmpFavorites, movie);
    }
  };

  const onSearch = (text: string): void => {
    if (text && text.length > 2) {
      setSearchText(text);
      dispatch(fetchMovieSearchRequest({ searchText: text, page }));
    } else {
      dispatch(deleteMovieSearch());
    }
  };

  const loadMore = (): void => {
    if (pending) {
      return;
    }

    dispatch(fetchMoreMoviesRequest({ searchText, page: page + 1 }));
    setPage(page + 1);
  };

  return (
    <ContentLayout
      children={
        <>
          <Card>
            <Search
              placeholder="Search for movie title..."
              enterButton="Search"
              size="large"
              onSearch={onSearch}
            />
          </Card>
          <>
            {pending ? (
              <div style={{ textAlign: "center", height: "100vh" }}>
                <Spin style={{ height: "50%" }} />
              </div>
            ) : error ? (
              <Result
                status="warning"
                title={error}
                extra={
                  <Link to="/" type="primary">
                    Back to home page
                  </Link>
                }
              />
            ) : movieSearch !== [] ? (
              <MovieCard
                totalResults={totalResults}
                page={page}
                favorites={favorites}
                data={movieSearch}
                loadMore={loadMore}
                handleIconClick={handleIconClick}
              />
            ) : (
              <Empty />
            )}
          </>
        </>
      }
    />
  );
};

export default Home;
