import ContentLayout from "../layout/ContentLayout";
import { Empty, Spin } from "antd";
import MovieCard from "../components/MovieCard";
import React, { useState } from "react";
import { IMovieSearch } from "../app/store/movieSearch/types";
import {
  getAllMovieLocalStorage,
  insetToLocalStorage,
  removeFromLocalStorage,
} from "../utils/localStorage";

const Favorites = () => {
  const [favorites, setFavorites] = useState<IMovieSearch[]>(
    getAllMovieLocalStorage()
  );

  const [pending, setPending] = useState<boolean>(false);

  const handleIconClick = (movie: IMovieSearch): void => {
    setPending(true);

    const exist = favorites.some(
      (favorite) => favorite.imdbID === movie.imdbID
    );
    if (exist) {
      const tmpFavorites = favorites.filter(
        (favorite) => favorite.imdbID !== movie.imdbID
      );
      setFavorites(tmpFavorites);

      removeFromLocalStorage(tmpFavorites, movie);
    } else {
      const tmpFavorites = favorites;
      tmpFavorites.push(movie);
      setFavorites([...tmpFavorites]);

      insetToLocalStorage(tmpFavorites, movie);
    }

    setPending(false);
  };

  return (
    <ContentLayout
      title="My favorite movies"
      children={
        <>
          {pending ? (
            <div style={{ textAlign: "center", height: "100vh" }}>
              <Spin style={{ height: "50%" }} />
            </div>
          ) : favorites !== [] ? (
            <MovieCard
              totalResults={favorites.length}
              page={0}
              favorites={favorites}
              data={favorites}
              loadMore={() => {}}
              handleIconClick={handleIconClick}
            />
          ) : (
            <Empty />
          )}
        </>
      }
    />
  );
};

export default Favorites;
