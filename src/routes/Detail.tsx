import ContentLayout from "../layout/ContentLayout";
import { useDispatch, useSelector } from "react-redux";
import { getMovieDetailSelector } from "../app/store/movieDetail/selectors";
import { useEffect } from "react";
import { fetchMovieDetailRequest } from "../app/store/movieDetail/actions";
import { Link, useParams } from "react-router-dom";
import { Empty, Result, Spin } from "antd";
import DetailDescription from "../components/DetailDescription";

const Detail = () => {
  const { imdbID } = useParams();

  const dispatch = useDispatch();
  const { error, movieDetail, pending } = useSelector(
    getMovieDetailSelector
  ).movieDetail;

  useEffect(() => {
    if (imdbID && imdbID !== "") dispatch(fetchMovieDetailRequest({ imdbID }));
  }, [imdbID]);

  return (
    <ContentLayout
      title={movieDetail ? movieDetail.Title : ""}
      children={
        <>
          {pending ? (
            <div style={{ textAlign: "center", height: "100vh" }}>
              <Spin style={{ height: "50%" }} />
            </div>
          ) : error ? (
            <Result
              status="warning"
              title={error}
              extra={
                <Link to="/" type="primary">
                  Back to homepage
                </Link>
              }
            />
          ) : movieDetail ? (
            <DetailDescription movieDetail={movieDetail} />
          ) : (
            <Empty />
          )}
        </>
      }
    />
  );
};

export default Detail;
