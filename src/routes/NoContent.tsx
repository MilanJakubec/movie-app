import ContentLayout from "../layout/ContentLayout";
import { Result } from "antd";
import { Link } from "react-router-dom";

const NoContent = () => {
  return (
    <ContentLayout
      title="Page do not exist"
      children={
        <Result
          status="404"
          title="404"
          subTitle="We are sorry this page does not exist."
          extra={
            <Link to="/" type="primary">
              Back to home page
            </Link>
          }
        />
      }
    />
  );
};

export default NoContent;
